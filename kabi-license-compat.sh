#!/usr/bin/env bash
#
# Copyright 2022 Red Hat. All Rights Reserved.
# 
# 2022 ckalina@redhat.com
# 
# Licensed under the GNU General Public License v2.0 (the "License"). You may
# not use this file except in compliance with the License. You can obtain a
# copy at https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
#
# SPDX-License-Identifier: GPL-2.0

set -euo pipefail

CONFIG_PKGDIR=${CONFIG_PKGDIR:-http://mirror.stream.centos.org/9-stream/AppStream/%s/os/Packages/}

declare -Ag KERNEL_DEVEL

array_empty() {
	test $# -eq 0
}

cleanup() {
	array_empty ${KERNEL_DEVEL[@]+"${KERNEL_DEVEL[@]}"} && return
	rm -f "${KERNEL_DEVEL[@]}"
}

log() {
	case "${CONFIG_SILENT+n}" in
	"y")
		;;
	*)
		echo "$@"
		;;
	esac
}

fail_kmod() {
	echo "ERROR: $*" >&2
	exit 1
}

trap cleanup EXIT

usage() {
	cat <<-EOF
	Usage: $0 [KMOD...]"
	
	Given a list of kernel modules, this script checks license compatibility
	between the modules and a CentOS Stream 9 (C9S) kernel (downloaded from the
	public CentOS Stream 9 repository).

	If you module is licensed under a GPL compatible license, no further checks
	are necessary and the module passes inspection.

	Otherwise, it inspects immediate use of each kernel exported symbol in the
	kernel module and verifies that it's license is compatible with the module
	license. Offending symbols are reported.

	You may pass a list of kernel modules to the script. The set of modules may
	be build for different architectures, however, they must all be C9S supported.

	This utility is licensed under GPLv2.

	Note: This script works on undefined symbols only, cf. nm(1).
	EOF
}

test_kmod_exists() {
	[ -e "$1" ] || {
		echo "ERROR: file not found: $1." >&2
		return 1
	}
	[ -r "$1" ] || {
		echo "ERROR: cannot read file: $1." >&2
		return 1
	}
	return 0
}

test_kmod_license_compat() {
	local kmod="$1"

	license="$(modinfo -F license "$kmod")"
	[ -z "$license" ] && fail_kmod "Unable to read license"

	log -n "  * License: $license "

	case "$license" in
	*GPL*)
		log "(GPL-compatible, nothing to do)"
		return 0
		;;
	*)
		log "(GPL-incompatible)"
		return 1
		;;
	esac
}

test_kmod_symbols() {
	local kmod="$1"

	arch=$(modinfo -F vermagic "$kmod" \
	       | cut -f1 -d' ' \
	       | awk -F'.' '{print $(NF);}')

	pkgdir_arch=$(printf $CONFIG_PKGDIR $arch)

	if [ -z "${KERNEL_DEVEL[$arch]+x}" ]
	then
		KERNEL_DEVEL[$arch]=$(mktemp)
		log "  * Downloading $arch-baseline into ${KERNEL_DEVEL[$arch]}"
		rpm_basename=$(curl -Ls $pkgdir_arch \
			| grep -Po '<a href="\Kkernel-devel-[0-9].*rpm(?=")' \
			| tail -n1)
	else
		log "  * Using downloaded $arch-baseline from ${KERNEL_DEVEL[$arch]}"
	fi

	log "  * Using baseline kernel $rpm_basename for license compatibility check"

	SYMBOLS=($(comm -12 \
		<(nm -C --undefined-only "$kmod" \
		  | awk '{print $(NF); }' \
		  | sort \
		  | uniq) \
		<(curl -Ls $pkgdir_arch/$rpm_basename \
		  | rpm2cpio \
		  | cpio -i --to-stdout "*Module.symvers" 2> /dev/null \
		  | awk '$4 ~ /EXPORT_SYMBOL.*GPL.*/ {print $2; }' | sort | uniq)
	))

	array_empty ${SYMBOLS[@]+"${SYMBOLS[@]}"} && return

	log "  * The following symbols have incompatible licenses:"
	for sym in ${SYMBOLS[@]}
	do
		log "    $sym"
	done
}

test_kmod() {
	local kmod="$1"
	[ -n "$kmod" ] || return

	log " :: Testing kernel module: $kmod"
	test_kmod_exists "$1" || return 1
	test_kmod_license_compat "$1" && return 0
	test_kmod_symbols "$1"
}

# Tests:

LICENSES_COMPAT=("GPL" "GPL v2" "GPL and additional rights" "Dual BSD/GPL" "Dual MIT/GPL" "Dual MPL/GPL")
LICENSES_INCOMPAT=("Proprietary")

_test() (
d=$(mktemp -d)
cd $d

cat > Makefile <<"EOF"
obj-m += kmod.o
CFLAGS_kmod.o := $(CFLAGS)
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
EOF
cat > kmod.c <<"EOF"
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ktime.h>
#include <linux/kprobes.h>
int init_module(void)
{
        register_kprobe(NULL);
        return ktime_get() + ktime_get_boottime() + ktime_get_real();
}
void cleanup_module(void)
{
        unregister_kprobe(NULL);
        printk(KERN_INFO "Module unloaded.\n");
}
#define xstr(s) str(s)
#define str(s) #s
MODULE_LICENSE(xstr(LICENSE));
MODULE_DESCRIPTION("desc");
MODULE_AUTHOR("auth");
EOF

CONFIG_SILENT=y
for license in "${LICENSES_COMPAT[@]}"
do
	echo
	echo "Testing compat licenses skip: $license"
	make CFLAGS="-DLICENSE='$license'"
	if test_kmod_license_compat "kmod.ko"
	then
		echo "OK"
	else
		echo "Failed on $license"
		exit 1
	fi
	make clean &> /dev/null
done

make CFLAGS="-DLICENSE='$license'"
kmod="kmod.ko"
arch=$(modinfo -F vermagic "$kmod" \
       | cut -f1 -d' ' \
       | awk -F'.' '{print $(NF);}')
pkgdir_arch=$(printf $CONFIG_PKGDIR $arch)
rpm_basename=$(curl -Ls $pkgdir_arch \
	| grep -Po '<a href="\Kkernel-devel-[0-9].*rpm(?=")' \
	| tail -n1)

echo
echo "Symbols used: "
curl -Ls $pkgdir_arch/$rpm_basename \
| rpm2cpio \
| cpio -i --to-stdout "*Module.symvers" 2> /dev/null \
| grep -P "\t($(nm --undefined-only kmod.ko | awk '{print $(NF); }' | tr '\n' '|'))\t" \
| sort -k2,2

echo
echo "Symbols reported: "
test_kmod_symbols kmod.ko

rm -rf $d
cd /tmp
)

# Main:

[ $# -eq 0 ] && {
	usage
	exit 0
}

[ "$1" = "--test" ] && {
	_test
	exit
}

for file in "$@"
do
	test_kmod "$file"
done
