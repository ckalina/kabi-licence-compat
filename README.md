#  GPL Compatibility Check

This utility is licensed under GPLv2.

**Input.** One or more kernel modules built vs. one of the CentOS Stream 9 supported architectures. Different modules may be built vs. different architectures or kernel releases. The script will print basic usage information when ran without any arguments.

**Remark.** The script does not fail on GPL compatible modules.

**Assumption.** Supplied kernel modules will be assumed to be licensed under a non-GPL compatible license in the following text.

The script checks license compatibility between the modules and a CentOS Stream 9 (C9S) kernel. Non-compatible symbols are reported.

**Limitation 1.** If a module uses a symbol that's not present/exported in in the C9S kernel, the script does not attempt to find a suitable replacement. If the suitable replacement is GPL-licensed it is not reported.

**Limitation 2.** The script uses `nm(1)` to determine the list of undefined symbols. The set of symbols under inspection is a subset of these.

**Important.** The script downloads the latest `kernel-devel` RPM package from CentOS Stream 9 repository. It's crucial that a model of CentOS Stream 9 repository is provided to the script. You may provide a local repository if you wish by overriding the `$CONFIG_PKGDIR` variable. The default value is
```
http://mirror.stream.centos.org/9-stream/AppStream/%s/os/Packages/
```
where the format specifier `%s` stands for architecture (this will correspond to the architecture(s) of supplied module(s)). To test whether a local repository works as expected, you may a long option `--test` to the script.
